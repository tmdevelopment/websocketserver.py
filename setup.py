from setuptools import setup

setup(
    name='websocketserver',
    version='1.0',
    description='TMD Websocket server mainly for RPi',
    author='Hannes Mäehans',
    author_email='hannes@tmd.ee',
    packages=['src'],  # source dir
    install_requires=['pathlib',
                      'pyserial',
                      'websockets',
                      'RPi.GPIO',
                      'requests',
                      'keyboard',
                      'evdev',
                      'DMXEnttecPro',
                      'smbus2'
                      ]  # external packages as dependencies
)
