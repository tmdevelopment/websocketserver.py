# websocketserver.py

## Idea

All peripherals should be "dumb", only receive or send an action, therefore all devices connect to websocket server.
Messages are routed through websocketserver via identifier, eg if you were used to sending message like ``` {"action":"setDMXChannel", "channel":11, "value": 127} ``` to Arduino via COMWebsocketBridge.exe, you have to change your package to something like that:  ```  {"identifier":"arduino-something","data":{"action":"setDMXChannel", "channel":11, "value": 127} ```, and receiving messages have changed in a similar way, e.g. ``` {"action":"controller_ok"} ``` will now be received ```{"identifier":"arduino1", "data":{"action":"controller_ok"}}```.

Wrapping packages - identifiers are added and stripped in this project scope - do not have to make any changes in Arduino projects :).

![diagram](docs/diagram.png)

## config.json

Address and connected devices can be configured, only ws scheme is supported. More examples are provided below
```
{
    "websocketaddress":"ws://0.0.0.0:8181",
    "devices":{
            "identifier": "somethingunique",
            "connection": "gpio",
            "type": "Input",
            "args": {
                "pin": 10,
                "event": "rise"
            }
        }
    }
}
```

## setup.py
Install dependencies via
```
pip3 install setuptools
pip3 install .
```
NB! will fail on windows,

```
#run
python3.7 main.py
#or
py -3.7 main.py
```


### Device examples
Identifier is **optional**, but recommended


GPIO Input, rise event
```
{
    "identifier": "somethingunique",
    "connection": "gpio",
    "type": "Input",
    "args": {
        "pin": 10,
        "event": "rise"
    }
}

```

GPIO Input, fall event
```
{
    "identifier": "teine",
    "connection": "gpio",
    "type": "Input",
    "args": {
        "pin": 26,
        "event": "fall"
    }
}

```

GPIO Input, both events
```
{
    "identifier": "kolmas",
    "connection": "gpio",
    "type": "Input",
    "args": {
        "pin": 3,
        "event": "both"
    }
}
```

GPIO Input, polling
```
{
    "identifier": "neljas",
    "connection": "gpio",
    "type": "Input",
    "args": {
        "pin": 24,
        "event": "poll",
        "interval": 5000
    }
}
```

GPIO Output (PWM)
```
{
    "identifier": "somethingunique",
    "connection": "gpio",
    "type": "Output",
    "args": {
        "pin": 12,
        "event": "PWM",
        "Hz": 1000
    }
}
```
GPIO Output (untested)
```
{
    "identifier": "viies",
    "connection": "gpio",
    "type": "Output",
    "args": {
        "pin": 24,
        "event": "normal",
        "initial":0
    }
}
```

COM connection, eg Arduino<br>
* port - optional, will connect to first free port<br/>
* baud - optional, default value is 115 200
* readLine - optional, default value is true - if true reads until linefeed, otherwise reads with timeout
```
{
    "identifier": "arduino1",
    "connection": "serial",
    "type": "COM",
    "args": {
        "port": "COM6" | "/dev/ttyACM0"
        "baud": "9600",
        "readLine": true | false
    }
}
```

UHF reader, args:<br>
* port - optional <br>
* baud - optional, default 115 200 <br>
* power - power in dB, 0-30, optional, default 30
```
{
    "identifier": "uhf",
    "connection": "serial",
    "type": "UHF",
    "args": {
        "power": 30
    }
}
```

Keyboard, args:<br>
* subscribed_devices - optional, default [] - meaning all devices will be listened, eg /dev/input/event2 <br>
* timeout - optional, default 50 (ms), next character will be waited <br>
* end - optional, default 'enter' - if end character is received data will be sent immediately <br>
* buffer_max_length - optional, default 20 - after x number events data will be sent <br<>>
```
{
    "identifier": "smth",
    "connection": "keyboard",
    "type": "Keyboard"
}
```

usbdmx <br>
* controller - Serial port for the controller, if DMXEnttecPro has been installed can easily be identified with command ```python -m DMXEnttecPro.utils```<br>
```
{
    "identifier": "smth",
    "connection": "usbdmx",
    "type": "Usbdmx",
    "args": {
        "controller": "/dev/ttyUSB0"
    }
}
```
relay <br>
* data to send for interaction - action: turnOn/turnOff and value: relay number (1-4)<br>
```
{
    "identifier": "smth",
    "connection": "relay",
    "type": "Relay"
}
```
Neopixel, args:<br>
* pin - optional, default 21 - led output pin
* order - optional, default RGB - depending on neopixel leds either RGB, GRB, RGBW or GRBW
* count - optional, default 20 - number of leds
```
{
    "identifier": "Neopixel",
    "connection": "gpio",
    "type": "Neopixel",
    "args": {
        "pin": 21,
        "order": "RGB",
        "count": 20
    }
}
```
