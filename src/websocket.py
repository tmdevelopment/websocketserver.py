from threading import Thread
from datetime import datetime
import threading
import asyncio
import json
import websockets
import ssl
import pathlib
import logging
from urllib.parse import urlparse

logger = logging.getLogger(__name__)

logging.getLogger("websockets.protocol").level = logging.ERROR
logging.getLogger("websockets.server").level = logging.ERROR
logging.getLogger("asyncio").level = logging.ERROR


class websocket:

    onMessageCallback = None
    onUserConnectCallback = None

    USERS = set()

    def __init__(self, websocketaddress):
        result = urlparse(websocketaddress)
        self.scheme = result.scheme
        self.hostname = result.hostname
        self.port = result.port

    def sendallsync(self, msg):
        asyncio.new_event_loop().run_until_complete(self.sendall(msg))

    async def sendall(self, msg):
        if isinstance(msg, str) is False:
            msg = json.dumps(msg)
        logger.debug("SENDING\t\t\t" + msg)
        if self.USERS:
            await asyncio.wait([user.send(msg) for user in self.USERS])

    async def register(self, user):
        logger.info("New user\t\t\t%s:%s",
                    user.remote_address[0],
                    user.remote_address[1]
                    )
        self.USERS.add(user)
        if self.onUserConnectCallback is not None:
            if callable(self.onUserConnectCallback):
                await self.onUserConnectCallback(user)
        else:
            msg = {"action": "connected"}
            await user.send(json.dumps(msg))

    async def unregister(self, user):
        logger.info("user disconnected: %s:%s",
                    user.remote_address[0],
                    user.remote_address[1]
                    )
        self.USERS.remove(user)

    async def myServer(self, user, path):
        # register(user) sends user_event() to user

        await self.register(user)
        try:
            async for message in user:
                logger.debug("OnMessage\t\t" + str(message))
                if self.onMessageCallback is not None:
                    if callable(self.onMessageCallback):
                        self.onMessageCallback(message)
        finally:
            await self.unregister(user)

    def start_server(self):
        if(self.scheme == "wss"):
            ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
            cert = pathlib.Path(__file__).with_name("localhost.pem")
            ssl_context.load_cert_chain(cert)
            start_server_s = websockets.serve(self.myServer, self.hostname,
                                              self.port, ssl=ssl_context)
        else:
            start_server_s = websockets.serve(self.myServer, self.hostname,
                                              self.port)
        asyncio.get_event_loop().run_until_complete(start_server_s)

        self.thread = Thread(target=asyncio.get_event_loop().run_forever)
        self.thread.daemon = True
        self.thread.start()
        uri = self.scheme + "://" + self.hostname + ":" + str(self.port)
        logger.info("START\t\t\t" + uri)
