#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time
import json
from datetime import datetime
from threading import Thread
import logging
from .. import Device
import keyboard
import string
from evdev import InputDevice, list_devices

logger = logging.getLogger(__name__)


class Keyboard(Device):
    path_phys_map = {}
    subscribed_devices = []
    devices = {}
    timeout = 0
    end = None
    buffer_max_length = None

    def __init__(self,
                 subscribed_devices=[],
                 timeout=50,
                 end='enter',
                 buffer_max_length=20):

        for dev in map(InputDevice, list_devices()):
            self.path_phys_map[dev.path] = dev.phys

        self.subscribed_devices = subscribed_devices
        self.timeout = timeout
        self.end = end
        self.buffer_max_length = buffer_max_length
        # suoer init has to be the last call,
        # because it calls the is_device function
        super().__init__()

    def receive(self, obj):
        logger.warn("This class does not support receiving")
        # self.write(obj)

    def key_event_callback(self, e):
        if len(self.subscribed_devices) > 0:
            if e.device not in self.subscribed_devices:
                return

        # currently listening only down events
        if e.event_type == 'up':
            return

        if e.device is None:
            e.device = "windows-does-not-provide-device"

        if e.device not in self.devices:
            self.devices[e.device] = KeyboardDevice()

        self.devices[e.device].last_received = datetime.now()

        if self.end is not None:
            if e.name == self.end:
                self.flush(e.device)
            else:
                self.devices[e.device].buffer.append(e.name)
        elif self.timeout is not None:
            self.devices[e.device].buffer.append(e.name)
        else:
            self.flush(e.device)

    def flush(self, device):
        msg = {"data": "".join(self.devices[device].buffer),
               "device": device,
               "phys": self.path_phys_map[device]}
        self.send(msg)
        self.devices[device].buffer = []
        self.devices[device].last_received = None

    def run(self):
        while True:
            time.sleep(0.01)
            try:
                keyboard.hook(self.key_event_callback)
                # keyboard.wait()
                while True:
                    self.timeout_check()

            except Exception as r:
                logger.info(r)
                time.sleep(2)

    def timeout_check(self):
        if self.timeout is None:
            time.sleep(5)
        else:
            time.sleep(self.timeout/1001)
            for device in self.devices:
                keyboard_device = self.devices[device]
                if keyboard_device.last_received is not None:
                    delta = datetime.now() - keyboard_device.last_received
                    elapsed_millis = delta.total_seconds() * 1000
                    if elapsed_millis > self.timeout:
                        self.flush(device)
                if len(keyboard_device.buffer) >= self.buffer_max_length:
                    self.flush(device)

    def __del__(self):
        logging.info("\t\t__del__")


class KeyboardDevice():

    last_received = None
    buffer = []
