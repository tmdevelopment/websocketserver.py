
from __future__ import print_function
from .lib.relay_lib_seeed import *
from .. import Device
import logging

logger = logging.getLogger(__name__)


class Relay(Device):

    def __init__(self):
        super().__init__()

    def receive(self, obj):
        try:
            if obj["action"] == "turnOn":
                relay_on(obj["value"])
            elif obj["action"] == "turnOff":
                relay_off(obj["value"])

        except Exception as ex:
            logger.error(ex)
