import time
import os
from .. import Device
if os.name == "nt":
    from .. import GPIO
else:
    import RPi.GPIO as GPIO


GPIO.setmode(GPIO.BOARD)


class Input(Device):

    constant_polling = False

    def __init__(self, pin, event, interval=100):
        super().__init__()

        self.interval = interval
        self.pin = pin
        GPIO.setup(self.pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        if event == "poll":
            self.constant_polling = True
            return
        elif event == "both":
            GPIO.add_event_detect(pin, GPIO.BOTH, bouncetime=15)
        elif event == "rise":
            GPIO.add_event_detect(pin, GPIO.RISING, bouncetime=15)
        elif event == "fall":
            GPIO.add_event_detect(pin, GPIO.FALLING, bouncetime=15)
        GPIO.add_event_callback(pin, self.callback)

    def set_constant_polling(self, state):
        self.constant_polling = state

    def run(self):
        while 1:
            try:
                timetosleep = self.interval/1000
                if self.constant_polling:
                    value = GPIO.input(self.pin)
                    data = {"event": "poll", "pin": self.pin, "value": value}
                    msg = {"data": data}
                    self.send(msg)
                    time.sleep(timetosleep)
                else:
                    time.sleep(1)
            except Exception as ex:
                logger.error(ex)

    def callback(self, channel):
        if(channel == self.pin):
            if GPIO.input(self.pin):
                msg = {"data": {"event": "rising", "pin": self.pin}}
                self.send(msg)
            else:
                msg = {"data": {"event": "falling", "pin": self.pin}}
                self.send(msg)
