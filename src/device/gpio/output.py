import time
import os
from .. import Device
if os.name == "nt":
    from .. import GPIO
else:
    import RPi.GPIO as GPIO
import logging

logger = logging.getLogger(__name__)

GPIO.setmode(GPIO.BOARD)


class Output(Device):

    constant_polling = False

    def __init__(self, pin, event, initial=0, Hz=0, interval=100):
        super().__init__()

        self.interval = interval
        self.pin = pin
        if event=="PWM":
            GPIO.setup(self.pin, GPIO.OUT)
            self.p = GPIO.PWM(self.pin, Hz)
        else:
            self.p = None
            GPIO.setup(self.pin, GPIO.OUT)
            if initial == 1 or initial == 0:
                GPIO.output(self.pin, initial)

    def receive(self, obj):
        try:
            if self.p is not None:
                self.p.start(0)
                if obj['dc']>0:
                    self.p.ChangeDutyCycle(obj['dc'])
                else:
                    self.p.stop()
            else:
                GPIO.output(self.pin, obj['state'])
        except Exception as ex:
            logger.error(ex)
