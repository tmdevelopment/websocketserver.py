import time
import os
import board
import logging
import neopixel
from .. import Device
if os.name == "nt":
    from .. import GPIO
else:
    import RPi.GPIO as GPIO

logfile = "/var/log/websocketserver.log"
opened_ports = []
sws = {}

logging.basicConfig(
    format="%(asctime)s %(levelname)-8s %(name)-25s %(message)s",
    level=logging.DEBUG,
    datefmt="%Y-%m-%d %H:%M:%S",
    handlers=[
            logging.FileHandler(filename=logfile, mode='w'),
            logging.StreamHandler()
    ]
)
os.chmod(logfile, 644)

logger = logging.getLogger(__name__)


class Neopixel(Device):

    constant_polling = False

    def __init__(self, pin, order, count, interval=100):
        super().__init__()

        self.interval = interval
        self.pin = pin
        GPIO.setup(self.pin, GPIO.OUT)
        global pinID
        pinID = "D" + str(pin)
        global ORDER
        ORDER = setOrder(order)
        global COUNT
        COUNT = count
        global pixels
        pixels = neopixel.NeoPixel(getattr(board,pinID), COUNT, pixel_order=ORDER)
        global COLOR
        COLOR = (200,200,0)

    def receive(self, obj):
        # Coloring
        try:
            rgb = obj["color"]
            global COLOR
            COLOR = (rgb[0], rgb[1], rgb[2])
        except:
            pass
        # Clearing
        try:
            obj["clear"]
            clearLeds()
        except:
            pass
        # Order
        try:
            ORDER = setOrder(obj["order"])
        except:
            pass
        # setPin(obj)
        # Led 
        try:
            pixels[obj["led"]] = COLOR
        except:
            pass
        try:
            setLeds(obj["leds"])
        except:
            pass


def setLeds(leds):
    try:
        for x in leds:
            pixels[x] = COLOR
    except:
        print("Input formatted wrong, must be [1,2,4] etc")

def setOrder(obj):
    try:
        order = obj["order"]
        if order == "RGB":
            return neopixel.RGB
        elif order == "GRB":
            return neopixel.GRB
        else:
            return neopixel.RGB
    except:
        return neopixel.RGB

def setPin(obj):
    try:
        newpinID = "D" + str(obj["pin"])
        if newpinID != pinID:
            COUNT = obj["count"]
            pinID = newpinID
            global pixels
            pixels = neopixel.NeoPixel(getattr(board,pinID), COUNT, pixel_order=ORDER)
    except:
        pass

def clearLeds():
    pixels.fill((0,0,0))