from .. import Device
import logging
from DMXEnttecPro import Controller

logger = logging.getLogger(__name__)


class Usbdmx(Device):

    def __init__(self, controller):
        self.controller = controller
        self.dmx = Controller(self.controller)
        # suoer init has to be the last call,
        # because it calls the is_device function
        super().__init__()

    def receive(self, obj):
        try:
            if obj["action"] == "setAllChannel":
                self.dmx.set_all_channels(obj["value"])
            elif obj["action"] == "clearAll":
                self.dmx.clear_channels()
            else:
                if isinstance(obj["channel"], list):
                    for channel in obj["channel"]:
                        self.dmx.set_channel(
                            channel, obj["value"])
                else:
                    self.dmx.set_channel(
                        obj["channel"], obj["value"])
            self.dmx.submit()
        except Exception as ex:
            logger.debug("dmx failed with error"+str(ex))
