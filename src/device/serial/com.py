#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import serial
import json
from datetime import datetime
from threading import Thread
import re
import pathlib
import logging
from .. import Device

logger = logging.getLogger(__name__)

"""
idea for testing a baudrate
ser = serial.Serial('/dev/ttyAMA0')
ser.timeout = 0.5
for baudrate in ser.BAUDRATES:
    if 9600 <= baudrate <= 115200:
        ser.baudrate = baudrate
        ser.write(packet)
        resp = ser.read()
        if resp != '':
            break
if ser.baudrate > 115200:
    raise RuntimeError("Couldn't find appropriate baud rate!")
"""


class COM(Device):
    onRead = None
    port = ""
    watchdog_interval = None
    readLine = True

    def __init__(self, port, baud=115200, readLine = True):

        self.port = port
        self.baud = baud
        self.readLine = readLine
        self.ser = serial.Serial(self.port, self.baud)
        # suoer init has to be the last call,
        # because it calls the is_device function
        super().__init__()

    """
    an example how is_device could be overwritten
        def is_device(self):
            self.write("?")
            time.sleep(.125)
            h = []
            s = ""
            while self.ser.inWaiting():
                sr = self.ser.read()
                h.append(ord(sr))
                s += str(sr, 'utf-8')
                time.sleep(0.01)
            return "arduinouno" in s
    """

    def write(self, msg):
        if isinstance(msg, str) is False:
            msg = json.dumps(msg)
        # TypeError: unicode strings are not supported,
        # please encode to bytes: '{"action": "connected"}'
        self.ser.write(msg.encode())

    def receive(self, obj):
        self.write(obj)

    def run(self):
        last_received = datetime.now()
        numberOfFailures = 0
        if self.ser is None:
            self.ser = serial.Serial(self.port, self.baud)
            time.sleep(.5)

        while self.alive:
            time.sleep(0.01)
            try:
                if self.ser is None:
                    self.ser = serial.Serial(self.port, self.baud)

                h = []
                s = ""
                while self.ser.inWaiting():
                    if self.readLine:
                        s = self.ser.readline().decode("utf8")
                    else:
                        sr = self.ser.read()
                        h.append(ord(sr))
                        s += str(sr, "utf-8")
                        time.sleep(0.01)

                if(len(s) > 0):

                    last_received = datetime.now()

                    s = s.strip()
                    logger.info(self.identifier + " RECEIVED:\t" + s)
                    try:
                        s = json.loads(s)
                    except Exception as ex:
                        logger.debug("json.loads failed with '" + s + "'")
                    msg = {"data": s}
                    self.send(msg)

                if self.watchdog_interval is not None:
                    delta = datetime.now() - last_received
                    if delta.total_seconds() > self.watchdog_interval:
                        raise Exception("Watchdog timeout expired")

            except Exception as E:
                numberOfFailures += 1
                if self.ser is not None:
                    self.ser.close()
                    self.ser = None
                msg = self.identifier + ":\texception, closing conn. " + str(E)
                logger.info(msg)
                time.sleep(2)
                if numberOfFailures > 1:
                    self.alive = False

    def __del__(self):
        logging.info(self.port + ":__del__")
