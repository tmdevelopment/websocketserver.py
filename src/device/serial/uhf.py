# !/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import serial
import json
from datetime import datetime
import pathlib
import logging
from .com import COM

logger = logging.getLogger(__name__)


class UHF(COM):

    basic_params = [
        0x1E,  # 0 PW = 30dBi (0x1E) - Power size
        0x01,  # 1 FHE = Enabled (0x01) - Frequenxy hopping enabled
        0x6E,  # 2 FFV = 915MHz (0x6E) - Fixed frequency
        0x54,  # 3 Frequency hopping 1
        0x5D,  # 4 Frequency hopping 2
        0x66,  # 5 Frequency hopping 3
        0x6F,  # 6 Frequency hopping 4
        0x78,  # 7 Frequency hopping 5
        0x82,  # 8 Frequency hopping 6
        0x02,  # 9 WM = Command (0x01) - Work Mode  USED, 0x02 -
               # Active and OM 0x01 is used, its much easier
        0x0A,  # 10 RI = 10ms (0x0A) - Read Interval
        0x00,  # 11 TGR = Disenabled (0x00) - Trigger
        0x01,  # 12 OM = RS232 (0x01) - Output Mode,
               # will out put constantly data,
               # unfortunately in the beginning used mode 6,
               # then have to be manually "triggerd"
        0x00,  # 13 WG Offset 0x00 - Wiegand offset
        0x1E,  # 14 WG interval 0x1E
        0x0A,  # 15 WG width
        0x0F,  # 16 WG period
        0x01,  # 17 AN = 1 (0x01) - Choice Antenna
        0x10,  # 18 RT = EPC (GEN 2) single tag (0x10) - Read Type
        0x01,  # 19 SI = 1s (0x01) - Same id Interval
        0x00,  # 20 BZ - DisEnabled (0x00) - Buzzer
        0x03,  # 21 UD MB=User - memory bank (UD -user defined?)
        0x00,  # 22 UD SA=0 - starting address byte pointer (UD -user defined?)
        0x06,  # 23 UD DL=6 word - data length (UD -user defined?)
        0x00,  # 24 PE = Disenabled (0x00), Encryption enabled
        0x00,  # 25 PW 0x0000
        0x00,  # 26 PW 0x0000
        0x20,  # 27 MR = 32 (0x20)
    ]

    multiple_tag_detection = False

    def __init__(self, port, baud=9600, power=0x1E, buzzer=0x00,
                 multiple_tag_detection=False, same_id_interval=1):
        if buzzer in range(0, 2):
            self.basic_params[20] = buzzer

        if power in range(0, 0x1E+1):
            self.basic_params[0] = power
        else:
            logger.warn("Given power not in range, value %s, defaulting to %s",
                        power, self.basic_params[0])

        if type(multiple_tag_detection) is bool:
            if multiple_tag_detection is True:
                self.multiple_tag_detection = multiple_tag_detection
                self.basic_params[9] = 0x01

        # seems not to be working, why?
        if same_id_interval in range(0, 100+1):
            self.basic_params[19] = same_id_interval
        super().__init__(port, baud)

    def is_device(self):
        result = self.is_reader()
        logger.info("IS Device: " + str(result))
        return result

    def is_reader(self):
        return self.send_set_basic_parameters() is not False

    def calc_checksum(self, data):
        s = sum(data)
        t = s % 256
        return 256-t

    def check_checksum(self, data):
        if data[3] == 0x11:
            # has a different checksum logic will check it later on
            return data[4] == 0x00
        else:
            return data[-1] == self.calc_checksum(data[:-1])

    def send_cmd(self, cid1, cid2, data=[], read=True, debug=False):
        # HEAD, ADDR(LSB), ADDR(MSB), CID1, CID2, LENGTH, CHKSUM
        data = [0x7C, 0xFF, 0xFF, cid1, cid2, len(data)] + data
        data.append(self.calc_checksum(data))

        if debug:
            s = ""
            for m in data:
                s += '{0:0{1}X}'.format(m, 2) + " "
            print("Sending: " + str(s))

        self.ser.write(data)
        if read:
            time.sleep(0.1)
            # s = self.ser.readline()
            # h = []
            int_list = []
            hex_string_list = []
            string = ""
            while self.ser.inWaiting():
                sr = self.ser.read()
                i = ord(sr)
                if not int_list and i != 0xCC:
                    continue
                int_list.append(i)
                # hex_string_list.append(hex(i))
                hex_string_list.append('{0:0{1}X}'.format(i, 2))
                time.sleep(0.025)
            if debug:
                print("RECEIVED: " + string.join("", hex_string_list))
                print("CHECKSUM: " + str(self.check_checksum(int_list)))
            if len(int_list) > 1 and self.check_checksum(int_list):
                return [int_list, hex_string_list, string]
            else:
                return False

    def send_information(self):
        r = self.send_cmd(0x82, 0x32)
        if r is not False:
            print(r[1])

    def send_BASE_parameters(self):
        r = self.send_cmd(0x81, 0x32)
        if r is not False:
            print(r[1])

    def send_set_basic_parameters(self):
        return self.send_cmd(0x81, 0x31, self.basic_params)

    def send_identify_single_tag(self):
        result = []
        data = self.send_cmd(0x10, 0x32)
        if data is not False:
            i = data[0]
            h = data[1]
            rtn = i[4]
            if rtn == 0x00:
                data_length = i[5]
                antenna_index = i[6]
                # 6. position is antenna_index, therefore -1
                start = 7
                end = start + data_length
                result.append(str.join("", h[start+1:end-1]))

    def send_identify_multiple_tag(self):
        result = []
        response = self.send_cmd(0x11, 0x32)
        if response is not False:
            i = response[0]
            h = response[1]
            tag_count = i[5]  # TC
            single_tag_data_length = i[6]  # DL
            for tag_indx in range(tag_count):
                start = 7 + tag_indx * single_tag_data_length
                end = start + single_tag_data_length
                single = i[start:end]
                if single[-1] == self.calc_checksum(single[:-1]):
                    singleHex = h[start+1:end-1]
                    result.append(str.join("", singleHex))
        return result

    def run(self):
        time.sleep(0.5)
        # self.send_cmd(params)
        while True:
            # time.sleep(.25)

            try:
                if self.ser is None:
                    self.ser = serial.Serial(self.port, self.baud)
                    self.send_set_basic_parameters()

                if self.multiple_tag_detection is True:
                    time.sleep(.05)
                    r = self.send_identify_multiple_tag()
                    if r:
                        msg = {"event": "read", "value": r}
                        self.send(msg)
                else:
                    time.sleep(.05)
                    received_string = []
                    int_list = []
                    while self.ser.inWaiting():
                        sr = self.ser.read()
                        i = ord(sr)
                        if not int_list and i != 0xCC:
                            continue
                        int_list.append(i)
                        received_string.append('{0:0{1}X}'.format(ord(sr), 2))
                        time.sleep(0.005)
                    if len(int_list) > 1 and self.check_checksum(int_list):
                        card_hex = received_string[7:-1]
                        msg = {"event": "read", "value": ["".join(card_hex)]}
                        self.send(msg)

            except Exception as E:
                if self.ser is not None:
                    self.ser.close()
                    self.ser = None
                else:
                    logger.error("Reader " + self.port + " lost")
                time.sleep(2)

    def __del__(self):
        logger.info("__del__")
