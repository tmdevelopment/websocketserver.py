#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import serial
import json
from datetime import datetime
from threading import Thread
import re
import pathlib
import logging
from .com import COM

logger = logging.getLogger(__name__)


class Meanwell3310UG(COM):
    def __init__(self, port, baud=115200):
        raise NotImplementedError
        super().__init__(port, baud)

    def is_device(self):
        return self.is_reader()

    def run(self):
        while 1:
            time.sleep(1)
            logger.info("run while 1 loop")

    def send_cmd(self, cmd, data="", read=True):
        to_send = [0x16, ord(cmd), 0x0D]
        if len(data):
            for d in data:
                to_send.append((ord(d)))
            to_send.append(ord("."))

        # _d(to_send)
        self.ser.write(to_send)
        if read:
            # s = self.ser.readline();
            # h = []
            time.sleep(0.4)
            h = []
            s = ""
            while self.ser.inWaiting():
                sr = self.ser.read()
                h.append(ord(sr))
                s += str(sr, 'utf-8')
                time.sleep(0.01)
            # _d(h)
            return [h, s]

    def set_serial_trigger_mode(self, millis):
        millis = max(0, min(millis, 30000))
        self.send_cmd("M", "trgsto" + str(millis))

    def set_led_level(self, level):
        print("set led level to: " + str(level))
        self.send_cmd("M", "pwrnol" + str(level))

    def get_decoder_revision(self):
        h, s = self.send_cmd("M", "REV_DR.")
        return s

    def is_reader(self):
        s = self.get_decoder_revision()
        # return True
        print(s)
        return s.startswith("REV_DR")

    def trigger(self):
        self.send_cmd("T", [], False)
        self.lastTrigger = datetime.now()

    def save_direction(self, dir):
        global filename
        self.direction = dir
        contents = self.read_file()
        contents[self.port] = self.direction
        self.save_file(contents)

    def read_direction(self):
        contents = self.read_file()
        if self.port in contents:
            self.direction = contents[self.port]

    def read_file(self):
        global filename
        if os.path.exists(filename):
            try:
                file = open(filename, "r")
                try:
                    contents = json.loads(file.read())
                    return contents
                except Exception as Ex1:
                    print(Ex1)
            except Exception as Ex2:
                print(Ex2)
        return {}

    def save_file(self, contents):
        jsond = json.dumps(contents)
        file = open(filename, "w")
        file.write(jsond)
        file.close()

    def run(self):
        led_level = 100
        if self.ser is None:
            self.ser = serial.Serial(self.port, self.baud)
        self.set_led_level(led_level)

        self.trigger()
        while True:
            try:
                if self.ser is None:
                    self.ser = serial.Serial(self.port, self.baud)
                    self.set_led_level(led_level)
                    self.trigger()

                last_trigger_delta = datetime.now() - self.lastTrigger
                if last_trigger_delta.total_seconds() > 29:
                    self.trigger()

                received_string = ""
                while self.ser.inWaiting():
                    received_string += str(self.ser.read(), 'utf-8')
                    time.sleep(0.01)

                # exit is 4 characters, used to get new line feed in the
                # office as well, but not with the new readers
                if len(received_string) > 3:
                    received_string = received_string.replace("\r", "")
                    received_string = received_string.replace("\n", "")

                    barcode = received_string.rstrip()
                    print(barcode)
                    self.trigger()

                if self.last_second != datetime.now().second:
                    if datetime.now().second % 10 == 0:
                        self.last_second = datetime.now().second
                        print(datetime.now())

            except Exception as E:
                if self.ser is not None:
                    self.ser.close()
                    self.ser = None
                print("Reader " + self.port + "lost")
                print(E)
                time.sleep(2)

    def __del__(self):
        print("__del__")
