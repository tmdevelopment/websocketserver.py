#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import serial
import json
from datetime import datetime
from threading import Thread
import re
import pathlib
import logging
from .. import Device
from .com import COM
logger = logging.getLogger(__name__)


class Arduino(COM):
    def __init__(self, port, baud=115200, watchdogInterval=None):
        super().__init__(port, baud=baud)
        self.watchdog_interval = watchdogInterval

    def is_device(self):
        for i in range(0, 50):
            h = []
            s = ""
            if self.ser.inWaiting():
                while self.ser.inWaiting():
                    sr = self.ser.read()
                    h.append(ord(sr))
                    try:
                        s += str(sr, 'utf-8')
                    except Exception as ex:
                        logger.error(ex)
                    time.sleep(0.01)
                try:
                    json.loads(s.strip())
                    if "action" in s:
                        logger.info("Found device")
                        return True
                except Exception as ex:
                    logger.warning(ex)
                    logger.info(s)
            else:
                time.sleep(0.1)
        self.ser.close()
        return False
