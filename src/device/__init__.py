from threading import Thread
import logging
import os
if os.name == "nt":
    import random

    class GPIO():
        PUD_DOWN = 0
        PUD_UP = 1
        IN = 1
        OUT = 0
        BOARD = 1
        BCM = 0
        RISING = 0
        FALLING = 1
        BOTH = 2

        def __init__(self):
            print("dummyinit")

        def setmode(mode):
            print("dummymode")

        def setup(pin, mode, pull_up_down):
            print("dummysetup")

        def add_event_detect(pin, eventType, bouncetime=300):
            print("dummy_add_event_detect")

        def add_event_callback(pin, eventType, bouncetime=300):
            print("dummy_add_event_callback")

        def input(pin):
            return random.randint(0, 1)

        def output(pin, value):
            msg = "dummy_output value" + str(value) + " to pin" + str(pin)
            logger.info(msg)


logger = logging.getLogger(__name__)


class Device(Thread):
    wscallback = None
    identifier = ""
    alive = True

    def __init__(self):
        Thread.__init__(self)
        if self.is_device() is False:
            raise Exception("Its not a right device!")

    def is_device(self):
        return True

    def send(self, obj):
        if self.wscallback is not None and callable(self.wscallback):
            obj["identifier"] = self.identifier
            self.wscallback(obj)

    def receive(self, obj):
        logger.error("Receive override is missing: " + str(obj))
