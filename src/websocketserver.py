#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import time
import serial.tools.list_ports
import os.path
import os
import pathlib
import logging
import importlib
import sys
from websocket import websocket

logfile = "/var/log/websocketserver.log"
opened_ports = []
sws = {}

logging.basicConfig(
    format="%(asctime)s %(levelname)-8s %(name)-25s %(message)s",
    level=logging.DEBUG,
    datefmt="%Y-%m-%d %H:%M:%S",
    handlers=[
            logging.FileHandler(filename=logfile, mode='w'),
            logging.StreamHandler()
    ]
)
os.chmod(logfile, 644)

logger = logging.getLogger(__name__)

try:
    with open(pathlib.Path(__file__).with_name("config.json")) as jsonfile:
        config = json.load(jsonfile)
except Exception as config_exception:
    msg = "Could not load config file. Make sure 'config.json' file exists\n"
    msg += "\t"*6 + "'config.example.json' can be copied as an example"
    logger.error(msg)
    exit()


def getDevice(dev):
    global opened_ports
    logger.info(dev['type'])
    inst = None
    module = "device." + dev["connection"] + "." + dev["type"].lower()
    class_name = dev["type"]
    MyClass = getattr(importlib.import_module(module), class_name)
    try:
        # starting to use EAFP
        try:
            inst = MyClass(**dev["args"])
        except Exception as argsmissing:
            inst = None
            if dev["connection"] == "serial":
                for port in getPorts():
                    if port in opened_ports:
                        continue
                    try:
                        if "args" not in dev:
                            dev["args"] = {}
                        dev["args"]["port"] = port
                        inst = MyClass(**dev["args"])
                        logger.info("found port\t\t" + port)
                        break
                    except Exception as devException:
                        del dev["args"]["port"]
                        logger.warning("Port failed: " + str(devException))
                        exc_type, exc_obj, exc_tb = sys.exc_info()
                        filepath = exc_tb.tb_frame.f_code.co_filename
                        fname = os.path.split(filepath)[1]
                        print(exc_type, fname, exc_tb.tb_lineno)
            else:
                inst = MyClass()
    except Exception as e:
        inst = None
        msg = "Error while creating a device " + json.dumps(dev) + ":" + str(e)
        logger.error(msg)

    # if port is open then add to list
    if inst is not None:
        try:
            opened_ports.append(dev["args"]["port"])
        except Exception as notaport:
            logger.debug(notaport)

    return inst


def getPorts():
    global sws
    ports = []
    systemPorts = ["/dev/ttyAMA0", "COM1"]
    connectedPorts = []

    port_list = serial.tools.list_ports.comports()
    for element in port_list:
        if element.device not in systemPorts:
            connectedPorts.append(element.device)
    # for testing purposes were reversing it
    # connectedPorts.reverse()
    return connectedPorts


def websocketmessagehandler(data):
    global config
    logger.info("OnMessage:\t\t" + str(data))
    try:
        parsedData = json.loads(data)
        for device in config["devices"]:
            if device["identifier"] == parsedData["identifier"]:
                try:
                    device["instance"].receive(parsedData["data"])
                except Exception as receivePassEx:
                    logger.error(receivePassEx)
    except Exception as ex:
        msg = "Failed to parse websocket message, "
        msg += "expecting JSON, got: '" + data + "', "
        msg += "exception: " + str(ex)
        logger.error(msg)

    # if data["action"] != None:
    # for port in ports:
    #    port.write(data)


async def websocketUserConnected(user):
    global config
    msg = []
    for device in config["devices"]:
        if "instance" in device:
            msg.append(
                {
                    "identifier": device["identifier"],
                    "connection": device["connection"],
                    "type": device["type"],
                }
            )
    await user.send(json.dumps(msg))


def handleDevices(devices):
    global opened_ports
    key = 0
    for dev in devices:
        key += 1
        try:
            if dev["instance"] is not None and dev["instance"].alive is False:
                opened_ports.remove(dev["args"]["port"])
                dev["instance"].join()
                del dev["instance"]
                dev["instance"] = None

                msg = {
                    "action": "deviceLost",
                    "device": {
                            "identifier": dev["identifier"],
                            "connection": dev["connection"],
                            "type": dev["type"],
                    }
                }
                sws.sendallsync(msg)
        except Exception as ex:
            logger.debug(ex)
        if "instance" not in dev or dev["instance"] is None:
            handleDevice(dev, key)


def handleDevice(dev, key):
    global sws
    dev["instance"] = getDevice(dev)
    if dev["instance"] is not None:
        if "identifier" in dev:
            dev["instance"].identifier = dev["identifier"]
        else:
            identifier = dev["connection"] + "-" + dev["type"] + "-" + str(key)
            dev["identifier"] = identifier.lower()
            dev["instance"].identifier = dev["identifier"]
        dev["instance"].wscallback = sws.sendallsync
        dev["instance"].start()

        msg = {
            "action": "deviceConnected",
            "device": {
                    "identifier": dev["identifier"],
                    "connection": dev["connection"],
                    "type": dev["type"],
            }
        }
        sws.sendallsync(msg)


def mainloop():
    global sws
    try:
        sws = websocket(config["websocketaddress"])
        sws.onMessageCallback = websocketmessagehandler
        sws.onUserConnectCallback = websocketUserConnected
        sws.start_server()
        while True:
            try:
                if "devices" in config:
                    handleDevices(config["devices"])
            except Exception as e:
                logger.error(e)
            time.sleep(5)
    # for exiting from console (Ctrl+C)
    except KeyboardInterrupt:
        # for port in ports:
        #    print(port.port)
        print("done")

if __name__ == "__main__":
    mainloop()
